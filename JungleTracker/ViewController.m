//
//  ViewController.m
//  JungleTracker
//
//  Created by Logen Watkins on 4/20/13.
//  Copyright (c) 2013 Logen Watkins. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "CooldownView.h"
#import "SettingsViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@end

@implementation ViewController

@synthesize buttonArray = _buttonArray;
@synthesize resetAllButton = _resetAllButton;
@synthesize synthesizer = _synthesizer;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSLog(@"App launch");

    // Subviews need access to the navigation controller
    AppDelegate *myDelegate = (AppDelegate  *)[[UIApplication sharedApplication] delegate];
    [myDelegate setSuperNavController:self.navigationController];
    
    // User settings
    [self setUserSettings];
    
    // This array will be used for the "reset all timers" button
    _cooldownViewArray = [[NSMutableArray alloc] init];
    
    // This is a serial queue for all notifications
    _sharedQueue = dispatch_queue_create("sharedQueue", DISPATCH_QUEUE_SERIAL);
    
    // Set previous orientation on first load
    _previousOrientationWasPortrait = UIInterfaceOrientationIsPortrait([self interfaceOrientation]);
    
    // Create navigation bar
    [self setupNavigationBar];
    
    // Set up the speech synthesizer
    _synthesizer = [[AVSpeechSynthesizer alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    // Stop the phone from going to sleep
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    // Setup cell dimensions
    [self updateCellDimensions];
    
    // Create team labels (if they already exist, they won't be recreated)
    [self createAllBanners];
    
    // Position the team labels
    [self repositionEverythingIfNecessary];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // WATK not doing anything special yet. Can pass arguments here.
//    SettingsViewController *svc = (SettingsViewController *)segue.destinationViewController;
}

// All default user settings are set here
- (void) setUserSettings {
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    NSString *keyName;
    keyName = @"soundOn";
    if (![settings objectForKey:keyName]) {
        [settings setBool:YES forKey:keyName];
    }
    keyName = @"vibrateOn";
    if (![settings objectForKey:keyName]) {
        [settings setBool:YES forKey:keyName];
    }
    keyName = @"sixtySecondsVoiceNotification";
    if (![settings objectForKey:keyName]) {
        [settings setBool:YES forKey:keyName];
    }
    keyName = @"fortyFiveSecondsVoiceNotification";
    if (![settings objectForKey:keyName]) {
        [settings setBool:NO forKey:keyName];
    }
    keyName = @"thirtySecondsVoiceNotification";
    if (![settings objectForKey:keyName]) {
        [settings setBool:YES forKey:keyName];
    }
    keyName = @"fifteenSecondsVoiceNotification";
    if (![settings objectForKey:keyName]) {
        [settings setBool:NO forKey:keyName];
    }
    keyName = @"spawnVoiceNotification";
    if (![settings objectForKey:keyName]) {
        [settings setBool:YES forKey:keyName];
    }
}

- (void) setupNavigationBar {
    // Nav bar coloring, any options
    [[[self navigationController] navigationBar] setTintColor:UI_LIGHTGRAY2];
    [[UIBarButtonItem appearance] setTintColor: UI_STEELBLUE_1];
    
    // Reset button options
    [[_resetAllButton titleLabel] setFont:[UIFont fontWithName:@"Futura-Medium" size:18.0]];
    [_resetAllButton setBackgroundImage:[UIImage imageNamed:@"PlainButton.png"] forState:UIControlStateNormal];
    [_resetAllButton setBackgroundColor:[UIColor clearColor]];
    
    // Settings button options
    UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [settingsButton setFrame:CGRectMake(0, 0, 24, 24)];
    [settingsButton setImage:[UIImage imageNamed:@"gear_24.png"] forState:UIControlStateNormal];
    [settingsButton addTarget:self action:@selector(settingsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:settingsButton];
    self.navigationItem.rightBarButtonItem = rightButton;
}

- (UILabel *) createBanner:(NSString *)title frame:(CGRect)frame {
    // Section labels
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text = title;
    [label setTextColor:[UIColor whiteColor]];
    [label setFont:[UIFont fontWithName:@"Walkway UltraBold" size:20.0f]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.8f]];
    [[self view] addSubview:label];
    return label;
}

- (void) createAllBanners {
    // Position frames for all labels
    [self updateBannerFramesForOrientation:[self interfaceOrientation]];

    // Red team
    if (nil == _redTeamLabel) {
        _redTeamLabel = [self createBanner:@"RED TEAM" frame:_redTeamLabelFrame];
    }
    
    // River
    if (nil == _riverLabel) {
        _riverLabel = [self createBanner:@"RIVER" frame:_riverLabelFrame];
    }
    
    // Blue team
    if (nil == _blueTeamLabel) {
        _blueTeamLabel = [self createBanner:@"BLUE TEAM" frame:_blueTeamLabelFrame];
    }
}

- (void) updateBannerFramesForOrientation:(UIInterfaceOrientation)orientation {
    CGSize winSize = [[UIScreen mainScreen] bounds].size;
    CGFloat labelWidth = winSize.width * 0.4;
    CGFloat labelHeight = 20.0;
    
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        // Following is used to center the label on the screen
        CGFloat labelXOffset = (winSize.width / 2) - (labelWidth / 2);
        
        // Red team
        _redTeamLabelFrame = CGRectMake(labelXOffset, 0, labelWidth, labelHeight);
        
        // River
        _riverLabelFrame = CGRectMake(labelXOffset, _cellHeight, labelWidth, labelHeight);
        
        // Blue team
        _blueTeamLabelFrame = CGRectMake(labelXOffset, _cellHeight * 2, labelWidth, labelHeight);
    } else {
        // Following is used to center the label within each cell
        CGFloat labelXOffset = (_cellWidth / 2) - (labelWidth / 2);
        
        // Red team
        _redTeamLabelFrame = CGRectMake(labelXOffset, 0, labelWidth, labelHeight);
        
        // River
        _riverLabelFrame = CGRectMake(labelXOffset + _cellWidth, 0, labelWidth, labelHeight);
        
        // Blue team
        _blueTeamLabelFrame = CGRectMake(labelXOffset + (_cellWidth * 2), 0, labelWidth, labelHeight);
    }
    
}

- (void) positionAllBannersForOrienation:(UIInterfaceOrientation)orientation {
    [self updateBannerFramesForOrientation:orientation];
    [UIView animateWithDuration:0.5f
                     animations:^ {
                         [_redTeamLabel setFrame: _redTeamLabelFrame];
                         [_riverLabel setFrame:_riverLabelFrame];
                         [_blueTeamLabel setFrame:_blueTeamLabelFrame];
                     }];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{    
    [super touchesBegan:touches withEvent:event];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)resetAllButtonPressed:(id)sender {    
    // TestFlight checkpoint
    [TestFlight passCheckpoint:@"ResetAll Pressed"];
    
    // Reset all the timers
    for (id cell in self.collectionView.visibleCells) { 
        //TEMP
        [cell tempChangeCooldown:5];
        //TEMP
        [cell timerResetWithGlow:NO];
        [cell updateCooldownLabel];
    }
}
    
- (IBAction)settingsButtonPressed:(id)sender {
    // TestFlight checkpoint
    [TestFlight passCheckpoint:@"Settings icon pressed"];
    [self performSegueWithIdentifier:@"settingsSegue" sender:self];
}

#pragma mark - UICollectionView Datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    // WATK TEMP
    return 6;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    // WATK TEMP
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CooldownView *cell = (CooldownView *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CooldownCell" forIndexPath:indexPath];
    NSString *cooldownTitle = nil;
    NSString *teamOverlayText = nil;
    NSString *audioPrefix = nil;
    CGFloat cooldown = 5;
    UIColor *normalColor = [UIColor blueColor];
    
    // Setup cell
    switch (indexPath.item) {
        case 0:
            cooldownTitle = @"LIZARD";
            teamOverlayText = @"Red team's";
            audioPrefix = @"Red_Red";
            cooldown = RED_COOLDOWN;
            normalColor = UI_RED_2;
            break;
        case 1:
            cooldownTitle = @"GOLEM";
            teamOverlayText = @"Red team's";
            audioPrefix = @"Red_Blue";
            cooldown = BLUE_COOLDOWN;
            normalColor = UI_STEELBLUE_1;
            break;
        case 2:
            cooldownTitle = @"BARON";
            teamOverlayText = nil;
            audioPrefix = @"Baron";
            cooldown = BARON_COOLDOWN;
            normalColor = [UIColor purpleColor];
            break;
        case 3:
            cooldownTitle = @"DRAGON";
            teamOverlayText = nil;
            audioPrefix = @"Dragon";
            cooldown = DRAGON_COOLDOWN;
            normalColor = UI_DARKORANGE_1;
            break;
        case 4:
            cooldownTitle = @"GOLEM";
            teamOverlayText = @"Blue team's";
            audioPrefix = @"Blue_Blue";
            cooldown = BLUE_COOLDOWN;
            normalColor = UI_STEELBLUE_1;
            break;
        case 5:
            cooldownTitle = @"LIZARD";
            teamOverlayText = @"Blue team's";
            audioPrefix = @"Blue_Red";
            cooldown = RED_COOLDOWN;
            normalColor = UI_RED_2;
            break;
        default:
            NSLog(@"Oh no! Error!");
    }

    // Set up the cell
    [cell cellSetupWithTitle:cooldownTitle
             teamOverlayText:teamOverlayText
                 audioPrefix:audioPrefix
                    cooldown:cooldown
                 normalColor:normalColor
                 sharedQueue:_sharedQueue
            audioSynthesizer:_synthesizer];
    
    // Update the frames now that it's been created
    [cell updateFramesForOrientation:[self interfaceOrientation]];
    
    cell.backgroundColor= [UIColor clearColor];
    return cell;
}

// for headers/footers (like red label)
//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
//    return [[UICollectionReusableView alloc] init];
//}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Select Item
    NSLog(@"sup");
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
    NSLog(@"nope");
}

#pragma mark - UICollectionViewDelegateFlowLayout
- (void) updateCellDimensions {
    // Cell sizes changes based on orientation
    CGSize collectionViewSize = [self.collectionView frame].size;
    if (UIInterfaceOrientationIsPortrait([self interfaceOrientation])) {
        _cellWidth = roundf(collectionViewSize.width / 2);
        _cellHeight  = roundf(collectionViewSize.height / 3);
    } else {
        _cellWidth = roundf(collectionViewSize.width / 3);
        _cellHeight  = roundf(collectionViewSize.height / 2);
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(_cellWidth, _cellHeight);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // No spacing between cells
    return UIEdgeInsetsZero;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

#pragma mark - Orientation change
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    // Why use both Will and Did? Think the animation might look cooler
    // temp
}

- (void)moveCellsForOrientation {
    
}

- (void)rotateCellsPortraitToLandscape {
    // Red-blue from 1 to 3
    [self.collectionView moveItemAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0] toIndexPath:[NSIndexPath indexPathForItem:3 inSection:0]];
    // Dragon from 3 to 4,
    [self.collectionView moveItemAtIndexPath:[NSIndexPath indexPathForItem:3 inSection:0] toIndexPath:[NSIndexPath indexPathForItem:4 inSection:0]];
}

- (void)rotateCellsLandscapeToPortrait {
    // Red-blue from 3 to 1
    [self.collectionView moveItemAtIndexPath:[NSIndexPath indexPathForItem:3 inSection:0] toIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]];
    // Dragon from 4 to 3,
    [self.collectionView moveItemAtIndexPath:[NSIndexPath indexPathForItem:4 inSection:0] toIndexPath:[NSIndexPath indexPathForItem:3 inSection:0]];
}

- (void) repositionEverything {
    UIInterfaceOrientation currentOrientation = [self interfaceOrientation];
    
    // Reposition the labels
    [self positionAllBannersForOrienation:currentOrientation];
    
    // Reposition the cells
    [self.collectionView performBatchUpdates:^{
        if (UIInterfaceOrientationIsPortrait(currentOrientation)) {
            [self rotateCellsLandscapeToPortrait];
        } else {
            [self rotateCellsPortraitToLandscape];
        }
    } completion:^(BOOL finished) {
        // Update the frames
        for (CooldownView *vc in [self.collectionView visibleCells]) {
            [vc updateFramesForOrientation:currentOrientation];
        }
    }];
}

- (void) repositionEverythingIfNecessary {
    UIInterfaceOrientation currentOrientation = [self interfaceOrientation];
    
    // Determine if the view should be repositioned, then set previousOrientation for the next call
    BOOL shouldReposition = (_previousOrientationWasPortrait ^ UIInterfaceOrientationIsPortrait(currentOrientation));
    _previousOrientationWasPortrait = UIInterfaceOrientationIsPortrait(currentOrientation);
    
    if (shouldReposition) {
        // Update cell dimensions
        [self updateCellDimensions];
        
        // Reposition
        [self repositionEverything];
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self repositionEverythingIfNecessary];
}

@end
