//
//  stdInclude.h
//  JungleTracker
//
//  Created by Logen Watkins on 5/30/13.
//  Copyright (c) 2013 Logen Watkins. All rights reserved.
//

#ifndef JungleTracker_stdInclude_h
#define JungleTracker_stdInclude_h

// Using this font many places
#define TITLE_FONT @"Walkway UltraBold"

// UIColor/ccc3 RGB
#define UI_RGB(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define CC_RGB(r,g,b) ccc3(r, g, b)

// Colors from: cloford.com/resources/colours/500col.htm
#define UI_AQUAMARINE UI_RGB(69, 139, 116, 1.0)
#define CC_AQUAMARINE CC_RGB(69, 139, 116)
#define UI_SLATEGRAY4 UI_RGB(108, 123, 139, 1.0)
#define CC_SLATEGRAY4 CC_RGB(108, 123, 139)
#define UI_STEELBLUE UI_RGB(70, 130, 180, 1.0)
#define CC_STEELBLUE CC_RGB(70, 130, 180)

#define UI_ROYALBLUE_3 UI_RGB(58, 95, 205, 1.0)
#define UI_RED_2 UI_RGB(238, 0, 0, 1.0)
#define UI_STEELBLUE_1 UI_RGB(28, 134, 238, 1.0)
#define UI_DARKORANGE_1 UI_RGB(255, 127, 0, 1.0)
#define UI_LIGHTGRAY UI_RGB(235, 235, 235, 1.0)
#define UI_LIGHTGRAY2 UI_RGB(223, 223, 223, 1.0)


#endif
