//
//  CooldownView.m
//  JungleTracker
//
//  Created by Logen Watkins on 4/21/13.
//  Copyright (c) 2013 Logen Watkins. All rights reserved.
//

#import "ViewController.h"
#import "CooldownView.h"
#import "QuartzCore/QuartzCore.h"
#import "AudioToolbox/AudioToolbox.h"
#import <TargetConditionals.h>
#import "AppDelegate.h"

@implementation CooldownView

@synthesize synthesizer = _synthesizer;

// WATK TEMP
- (void) tempChangeCooldown:(CGFloat)cooldown {
    _currentCooldown = arc4random() % 200;
    _maxCooldown = _currentCooldown;
}
// WATK TEMP

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        // For any standard setup
    }
    return self;    
}

- (void) updateFramesForOrientation:(UIInterfaceOrientation)orientation {
    // Create the alternate frames/positions, adjusting for orientation
    CGRect frame = self.frame;
    AppDelegate *appDelegate = (AppDelegate  *)[[UIApplication sharedApplication] delegate];

    CGFloat navBarHeight = [[[appDelegate superNavController] navigationBar] frame].size.height;
    CGSize winSize;
    CGFloat statusBarHeight;
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
        winSize.height = [[UIScreen mainScreen] bounds].size.height;
        winSize.width = [[UIScreen mainScreen] bounds].size.width;
    } else {
        statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.width;
        winSize.height = [[UIScreen mainScreen] bounds].size.width;
        winSize.width = [[UIScreen mainScreen] bounds].size.height;
    }
    
    _defaultFrame = frame;
    _overlayFrame = CGRectMake(0, 0, winSize.width, winSize.height - statusBarHeight - navBarHeight);
    _titleLowerFrame = CGRectMake(0, frame.size.height * 0.35, frame.size.width, frame.size.height * 0.3);
    _titleUpperFrame = CGRectMake(0, frame.size.height * 0.15, frame.size.width, frame.size.height * 0.3);
    _titleOverlayFrame = CGRectMake(0, _overlayFrame.size.height * 0.15, _overlayFrame.size.width , _overlayFrame.size.height * 0.3);
    _cooldownLowerFrame = CGRectMake(0, frame.size.height, frame.size.width, frame.size.height * 0.4);
    _cooldownUpperFrame = CGRectMake(0, _titleUpperFrame.origin.y + _titleUpperFrame.size.height, frame.size.width, frame.size.height * 0.4);
    _spawnedDefaultFrame = _cooldownUpperFrame;
    _spawnedOverlayFrame = CGRectMake(0, _overlayFrame.size.height * 0.45, _overlayFrame.size.width , _overlayFrame.size.height * 0.3);
    _teamOverlayFrame = CGRectMake(0, _overlayFrame.size.height * 0.12, _overlayFrame.size.width, _overlayFrame.size.height * 0.1);
    _teamDefaultFrame = CGRectMake(0, 0 - (_teamOverlayFrame.size.height), frame.size.width, frame.size.height * 0.4);
}

- (void)cellSetupWithTitle:(NSString *)cooldownTitle
           teamOverlayText:(NSString *)teamOverlayText
               audioPrefix:(NSString *)audioPrefix
                  cooldown:(CGFloat)cooldown
               normalColor:(UIColor *)normalColor
               sharedQueue:sharedQueue
          audioSynthesizer:synthesizer
{
    // Store variables
    _cooldownTitle = cooldownTitle;
    _teamOverlayText = teamOverlayText;
    _maxCooldown = cooldown;
    _currentCooldown = cooldown;
    _normalColor = normalColor;
    _audioPrefix = audioPrefix;
    _sharedQueue = sharedQueue;
    _synthesizer = synthesizer;
    
    // Default values
    _normalZPosition = 0.0;
    _parentLayerZPositionHide = 5.0;
    _overlayZPosition = 10.0;
    
    // Update the frames
    [self updateFramesForOrientation:UIInterfaceOrientationPortrait];
    
    // Title label
    _titleLabel = [self addUILabelWithFrame:_titleLowerFrame
                                       text:cooldownTitle
                                    visible:YES];
    
    // Cooldown label: below view, hidden to fade in later
    _cooldownLabel = [self addUILabelWithFrame:_cooldownLowerFrame
                                          text:nil
                                       visible:NO];
    [_cooldownLabel setFont:[UIFont fontWithName:TITLE_FONT size:44.0f]];
    
    // Spawned label:
    _spawnedLabel = [self addUILabelWithFrame:_spawnedDefaultFrame
                                         text:@"Spawned"
                                      visible:NO];
    
    // Team label
    _teamLabel = [self addUILabelWithFrame:_teamDefaultFrame
                                      text:_teamOverlayText
                                   visible:NO];
    
    // Background color is supplied for each view
    self.contentView.backgroundColor = _normalColor;
    
    // Make sure the cooldown label is set
    [self updateCooldownLabel];
}

- (UILabel *) addUILabelWithFrame:(CGRect)frame text:(NSString *)text visible:(BOOL)visible {
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    [label setText:text];
    [label setTextColor:[UIColor whiteColor]];
    [label setContentMode:UIViewContentModeScaleAspectFit];
    [label setFont:[UIFont fontWithName:TITLE_FONT size:22.0f]];
    [label setTextAlignment:NSTextAlignmentCenter];
        [label setBackgroundColor:[UIColor clearColor]];
    if (visible) {
        [label setAlpha:1.0];
    } else {
        [label setAlpha:0];
    }
    
    [self.contentView addSubview:label];
    return label;
}

- (void) setLabel:(UILabel *)label frame:(CGRect)frame fontSize:(CGFloat)fontSize visible:(BOOL)visible {
    [label setFrame:frame];
    [label setFont:[UIFont fontWithName:TITLE_FONT size:fontSize]];
    [label setAlpha:(visible) ? 1.0f : 0.0f];
}

- (void) viewDefaultFrames:(BOOL)animated withSound:(BOOL)sound {
    CGFloat animationDuration = 0;
    if (animated) {
        animationDuration = 0.7f;
    }

    [UIView animateWithDuration:animationDuration
                     animations:^{
                         // Set frames
                         [self setLabel:_titleLabel frame:_titleLowerFrame fontSize:22.0f visible:YES];
                         [self setLabel:_spawnedLabel frame:_spawnedDefaultFrame fontSize:22.0f visible:NO];
                         [self setLabel:_teamLabel frame:_teamDefaultFrame fontSize:22.0f visible:NO];
                         [self setFrame:_defaultFrame];
                     }
                     completion:^(BOOL finished){
                         [[self layer] setZPosition:_normalZPosition];
                         [[[self superview] layer] setZPosition:_normalZPosition];
                         [self glowingBackground];
                         [self timerResetWithGlow:YES];
                         
                         // If there is an associated sound, don't modify the sharedQueue.
                         // If there is no associated sound, resume the sharedQueue
                         if (!sound) {
                             dispatch_resume(_sharedQueue);
                         }
#if TARGET_IPHONE_SIMULATOR
                         else {
                             dispatch_resume(_sharedQueue);
                         }
#endif
                     }];
}

- (void) viewOverlayFramesWithSound:(BOOL)sound {
    [self timerStop];
    [self glowingBackground];
    [_cooldownLabel setAlpha:0];
    [[[self superview] layer] setZPosition:_parentLayerZPositionHide];
    [[self layer] setZPosition:_overlayZPosition];
    [UIView animateWithDuration:0.7f
                     animations:^{
                         // Change frames
                         [self setFrame:_overlayFrame];
                         [self setLabel:_titleLabel frame:_titleOverlayFrame fontSize:38.0f visible:YES];
                         [self setLabel:_spawnedLabel frame:_spawnedOverlayFrame fontSize:38.0f visible:YES];
                         [self setLabel:_teamLabel frame:_teamOverlayFrame fontSize:28.0f visible:YES];
                         
                         // Vibrate phone
                         [self vibratePhone];
                     }
                     completion:^(BOOL finished){
                         // This is the amount of time to keep the overlay where it is.
                         [NSThread sleepForTimeInterval:0.5];
                         [self viewDefaultFrames:YES withSound:sound];
                     }];
}

- (void) moveTitleUpAndShowCooldown {
    [UIView animateWithDuration:0.4f
                     animations:^{
                         [_titleLabel setFrame:_titleUpperFrame];
                         [_cooldownLabel setAlpha:1.0];
                         [_cooldownLabel setFrame:_cooldownUpperFrame];
                     }
                     completion:^(BOOL finished){
                     }];
}

- (void) moveTitleDownAndHideCooldown {
    [UIView animateWithDuration:0.4f
                     animations:^{
                         [_titleLabel setFrame:_titleLowerFrame];
                         [_cooldownLabel setAlpha:0];
                         [_cooldownLabel setFrame:_cooldownLowerFrame];
                     }
                     completion:^(BOOL finished){
                     }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
}*/

- (void) updateCooldownLabel {
    NSInteger minutes = 0;
    NSInteger seconds = 0;
    if (_currentCooldown > 0) {
        minutes = _currentCooldown / 60;
        seconds = _currentCooldown - (minutes * 60);
    }

    [_cooldownLabel setText:[NSString stringWithFormat:@"%d:%02d", minutes, seconds]];
}

- (void) timerStart {
    [self moveTitleUpAndShowCooldown];

    // Updating the label
    _currentCooldown = _maxCooldown;
    [self updateCooldownLabel];
    
    // Kick off the timer
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.5f
                                              target:self
                                            selector:@selector(timerFired)
                                            userInfo:nil
                                             repeats:YES];
}

- (void) timerStop {
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void) timerResetWithGlow:(BOOL)withGlow {
    [self timerStop];
    _currentCooldown = _maxCooldown;
    [self moveTitleDownAndHideCooldown];
    
    if (!withGlow) {
        [self normalBackground];
    }
}

- (void) startNotificationWithOverlay:(BOOL)overlay withSound:(BOOL)sound soundTime:(NSInteger)soundTime {
    if (overlay || sound) {
        // Start overlay notification
        dispatch_async(_sharedQueue, ^(void) {
            // Suspend the shared queue
#if !TARGET_IPHONE_SIMULATOR
            dispatch_suspend(_sharedQueue);
#else
            if (overlay) {
                dispatch_suspend(_sharedQueue);
            }
#endif
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                // Sounds for the above times (starting 1s early)
                if (sound) {
                    [self playSoundForTime:soundTime];
                }
                if (overlay) {
                    [self viewOverlayFramesWithSound:sound];
                }
            });
        });
    }
}

// Timer will fire every 0.5s
- (void) timerFired {
    // Decrement coodlown by 0.5s
    _currentCooldown -= TIME_INTERVAL;
    BOOL wholeNUMBER = NO;
    BOOL voiceNotification = YES;

    // On whole numbers (1.0, not 1.5)
    if (!(_currentCooldown - (NSInteger)_currentCooldown)) {
        wholeNUMBER = YES;
        
        switch ((int)_currentCooldown) {
            case 61:
            {
                // Play "1 minute" sound
                if (![[NSUserDefaults standardUserDefaults] boolForKey:@"sixtySecondsVoiceNotification"]) {
                    voiceNotification = NO;
                }
                [self startNotificationWithOverlay:NO
                                         withSound:voiceNotification
                                         soundTime:60];
                break;
            }
            case 46:
            {
                // Play "45 seconds" sound
                if (![[NSUserDefaults standardUserDefaults] boolForKey:@"fortyFiveSecondsVoiceNotification"]) {
                    voiceNotification = NO;
                }
                [self startNotificationWithOverlay:NO
                                         withSound:voiceNotification
                                         soundTime:45];
                break;
            }
            case 31:
            {
                // Play "30 seconds" sound
                if (![[NSUserDefaults standardUserDefaults] boolForKey:@"thirtySecondsVoiceNotification"]) {
                    voiceNotification = NO;
                }
                [self startNotificationWithOverlay:NO
                                         withSound:voiceNotification
                                         soundTime:30];
                break;
            }
            case 16:
            {
                // Play "15 seconds" sound
                if (![[NSUserDefaults standardUserDefaults] boolForKey:@"fifteenSecondsVoiceNotification"]) {
                    voiceNotification = NO;
                }
                [self startNotificationWithOverlay:NO
                                         withSound:voiceNotification
                                         soundTime:15];
                break;
            }
            case 1:
            {                
                // Play "has spawned" sound and
                // Show overlay screen
                if (![[NSUserDefaults standardUserDefaults] boolForKey:@"spawnVoiceNotification"]) {
                    voiceNotification = NO;
                }
                [self startNotificationWithOverlay:YES
                                         withSound:voiceNotification
                                         soundTime:0];
                break;
            }
                
            default:
                break;
        }
    }
    
    // Flash golden border when below 3s
    if (3.5 > _currentCooldown) {
        wholeNUMBER ? [self normalBackground] : [self glowingBackground];
    }
    
    // Update label
    [self updateCooldownLabel];
}

- (void) glowingBackground {
    self.layer.borderColor = [[UIColor yellowColor] CGColor];
    self.layer.borderWidth = 10.0f;
}

- (void) selectedBackground {
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.layer.borderWidth = 8.0f;
}

- (void) normalBackground {
    self.layer.borderWidth = 0.0f;
}

- (void)utterString:(NSString *)string {    
    AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:string];
    [utterance setRate:.25];
    [_synthesizer speakUtterance:utterance];
}

- (void) playSoundForTime:(NSInteger)currentTime {
    // Check if the user has sound enabled
    NSString *stringToSay;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"soundOn"]) {
        switch (currentTime) {
            case 0:
                // "X has spawned"
                stringToSay = [NSString stringWithFormat:@"%@ %@ has spawned", _teamOverlayText, _cooldownTitle];
                [self utterString:stringToSay];
                break;
                
            default:
                // "X will spawn in Y seconds"
                stringToSay = [NSString stringWithFormat:@"%@ %@ will spawn in %d seconds", _teamOverlayText, _cooldownTitle, currentTime];
                [self utterString:stringToSay];
                break;
        }
    }
}

- (void) playSystemSound:(SystemSoundID)soundID {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"soundOn"]) {
        AudioServicesPlaySystemSound(soundID);
    }
}

- (void) vibratePhone {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"vibrateOn"]) {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // Change background color on touch
    [self selectedBackground];
    
    if (_timer && _currentCooldown > 0) {
        // Stop the timer if already running
        [self timerResetWithGlow:NO];
    } else {
        // Reset the timer if still running
        if (_timer) {
            [self timerResetWithGlow:NO];
        }
        
        // Start the timer if not already running
        [self timerStart];

        // Decrementing so that user sees action immediately.
        // Timers are therefore TIME_INTERVAL seconds short        
        _currentCooldown -= TIME_INTERVAL;
    }

    // Play click so user knows they hit button
    [self playSystemSound:0x450];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    // Restore normal background color 0.2 seconds after
    [NSTimer scheduledTimerWithTimeInterval:0.2f
                                     target:self
                                   selector:@selector(normalBackground)
                                   userInfo:nil
                                    repeats:NO];
}

@end
