//
//  SettingsViewController.h
//  JungleTracker
//
//  Created by Logen Watkins on 4/26/13.
//  Copyright (c) 2013 Logen Watkins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController


@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewOutlet;
@property (weak, nonatomic) IBOutlet UILabel *sixtySecondsLabel;
@property (weak, nonatomic) IBOutlet UILabel *fortyFiveSecondsLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirtySecondsLabel;
@property (weak, nonatomic) IBOutlet UILabel *fifteenSecondsLabel;
@property (weak, nonatomic) IBOutlet UILabel *spawnLabel;
@property (weak, nonatomic) IBOutlet UISwitch *sixtySecondsSwitchOutlet;
@property (weak, nonatomic) IBOutlet UISwitch *fortyFiveSecondsSwitchOutlet;
@property (weak, nonatomic) IBOutlet UISwitch *thirtySecondsSwitchOutlet;
@property (weak, nonatomic) IBOutlet UISwitch *fifteenSecondsSwitchOutlet;
@property (weak, nonatomic) IBOutlet UISwitch *spawnSwitchOutlet;
- (IBAction)sixtySecondsSwitchAction:(id)sender;
- (IBAction)fortyFiveSecondsSwitchAction:(id)sender;
- (IBAction)thirtySecondsSwitchAction:(id)sender;
- (IBAction)fifteenSecondsSwitchAction:(id)sender;
- (IBAction)spawnSwitchAction:(id)sender;

- (IBAction)vibrateSwitchAction:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *vibrateSwitchOutlet;

@end
