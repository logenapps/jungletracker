//
//  ViewController.h
//  JungleTracker
//
//  Created by Logen Watkins on 4/20/13.
//  Copyright (c) 2013 Logen Watkins. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CooldownView.h"

// watk fix later
//#define RED_COOLDOWN (5*60)
//#define BLUE_COOLDOWN (5*60)
//#define DRAGON_COOLDOWN (6*60)
//#define BARON_COOLDOWN (7*60)

#define RED_COOLDOWN (5)
#define BLUE_COOLDOWN (17)
#define DRAGON_COOLDOWN (48)
#define BARON_COOLDOWN (77)

@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {
    NSMutableArray *_cooldownViewArray;
    dispatch_queue_t _sharedQueue;
    CGRect _redTeamLabelFrame;
    UILabel *_redTeamLabel;
    CGRect _riverLabelFrame;
    UILabel *_riverLabel;
    CGRect _blueTeamLabelFrame;
    UILabel *_blueTeamLabel;
    CGFloat _cellWidth;
    CGFloat _cellHeight;
    BOOL _previousOrientationWasPortrait;
}

@property (nonatomic) NSMutableArray *buttonArray;
@property (weak, nonatomic) IBOutlet UIButton *resetAllButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, atomic) AVSpeechSynthesizer *synthesizer;

- (IBAction)resetAllButtonPressed:(id)sender;

@end
