//
//  SettingsViewController.m
//  JungleTracker
//
//  Created by Logen Watkins on 4/26/13.
//  Copyright (c) 2013 Logen Watkins. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

// Removed the init code so that the storyboard nib is used
@synthesize sixtySecondsLabel = _sixtySecondsLabel;
@synthesize fortyFiveSecondsLabel = _fortyFiveSecondsLabel;
@synthesize thirtySecondsLabel = _thirtySecondsLabel;
@synthesize fifteenSecondsLabel = _fifteenSecondsLabel;
@synthesize spawnLabel = _spawnLabel;
@synthesize scrollViewOutlet = _scrollViewOutlet;
@synthesize sixtySecondsSwitchOutlet = _sixtySecondsSwitchOutlet;
@synthesize fortyFiveSecondsSwitchOutlet = _fortyFiveSecondsSwitchOutlet;
@synthesize thirtySecondsSwitchOutlet = _thirtySecondsSwitchOutlet;
@synthesize fifteenSecondsSwitchOutlet = _fifteenSecondsSwitchOutlet;
@synthesize spawnSwitchOutlet = _spawnSwitchOutlet;
@synthesize vibrateSwitchOutlet = _vibrateSwitchOutlet;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    // Navigation bar title
    UILabel *navTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [navTitleLabel setText:@"Settings"];
    [navTitleLabel setBackgroundColor:[UIColor clearColor]];
    [navTitleLabel setTextColor:UI_STEELBLUE_1];
    [navTitleLabel setFont:[UIFont fontWithName:@"Futura-Medium" size:24.0]];
    [navTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [[self navigationItem] setTitleView:navTitleLabel];
    [navTitleLabel sizeToFit];
    
    // Set the state of each switch based on user settings
    [self setDefaultsForKey:@"sixtySecondsVoiceNotification" forLabel:_sixtySecondsLabel forSwitch:_sixtySecondsSwitchOutlet];
    [self setDefaultsForKey:@"fortyFiveSecondsVoiceNotification" forLabel:_fortyFiveSecondsLabel forSwitch:_fortyFiveSecondsSwitchOutlet];
    [self setDefaultsForKey:@"thirtySecondsVoiceNotification" forLabel:_thirtySecondsLabel forSwitch:_thirtySecondsSwitchOutlet];
    [self setDefaultsForKey:@"fifteenSecondsVoiceNotification" forLabel:_fifteenSecondsLabel forSwitch:_fifteenSecondsSwitchOutlet];
    [self setDefaultsForKey:@"spawnVoiceNotification" forLabel:_spawnLabel forSwitch:_spawnSwitchOutlet];
    [self setDefaultsForKey:@"vibrateOn" forLabel:nil forSwitch:_vibrateSwitchOutlet];
    
    // To enable scrolling, have to set the scrollview size
    CGSize frameSize = self.view.frame.size;
    [_scrollViewOutlet setContentSize:CGSizeMake(frameSize.width, frameSize.height)];
}

- (void)setDefaultsForKey:(NSString *)keyName forLabel:(UILabel *)thisLabel forSwitch:(UISwitch *)thisSwitch {
    if (nil != keyName) {
        BOOL currentValue = [[NSUserDefaults standardUserDefaults] boolForKey:keyName];
        if (nil != thisLabel) {
            [self setColorOfLabel:thisLabel forValue:currentValue];
        }
        if (nil != thisSwitch) {
            [self setStateOfSwitch:thisSwitch forValue:currentValue];   
        }
    }
}

- (void)setStateOfSwitch:(UISwitch *)thisSwitch forValue:(BOOL)currentValue {
    [thisSwitch setOn:currentValue];
}

- (void)setColorOfLabel:(UILabel *)thisLabel forValue:(BOOL)currentValue {
    if (currentValue) {
        [thisLabel setTextColor:UI_STEELBLUE_1];
    } else {
        [thisLabel setTextColor:[UIColor redColor]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sixtySecondsSwitchAction:(id)sender {
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    BOOL switchValue = ((UISwitch *)sender).on ? YES : NO;
    [settings setBool:switchValue forKey:@"sixtySecondsVoiceNotification"];
    [self setColorOfLabel:_sixtySecondsLabel forValue:switchValue];
}

- (IBAction)fortyFiveSecondsSwitchAction:(id)sender {
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    BOOL switchValue = ((UISwitch *)sender).on ? YES : NO;
    [settings setBool:switchValue forKey:@"fortyFiveSecondsVoiceNotification"];
    [self setColorOfLabel:_fortyFiveSecondsLabel forValue:switchValue];
}

- (IBAction)thirtySecondsSwitchAction:(id)sender {
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    BOOL switchValue = ((UISwitch *)sender).on ? YES : NO;
    [settings setBool:switchValue forKey:@"thirtySecondsVoiceNotification"];
    [self setColorOfLabel:_thirtySecondsLabel forValue:switchValue];
}

- (IBAction)fifteenSecondsSwitchAction:(id)sender {
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    BOOL switchValue = ((UISwitch *)sender).on ? YES : NO;
    [settings setBool:switchValue forKey:@"fifteenSecondsVoiceNotification"];
    [self setColorOfLabel:_fifteenSecondsLabel forValue:switchValue];
}

- (IBAction)spawnSwitchAction:(id)sender {
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    BOOL switchValue = ((UISwitch *)sender).on ? YES : NO;
    [settings setBool:switchValue forKey:@"spawnVoiceNotification"];
    [self setColorOfLabel:_spawnLabel forValue:switchValue];
}

- (IBAction)vibrateSwitchAction:(id)sender {
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    BOOL switchValue = ((UISwitch *)sender).on ? YES : NO;
    [settings setBool:switchValue forKey:@"vibrateOn"];
}

@end
