//
//  CooldownView.h
//  JungleTracker
//
//  Created by Logen Watkins on 4/21/13.
//  Copyright (c) 2013 Logen Watkins. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#define TIME_INTERVAL (0.5)

@interface CooldownView : UICollectionViewCell {
    CGRect _defaultFrame;
    CGRect _overlayFrame;
    
    UILabel *_titleLabel;
    CGRect _titleLowerFrame;
    CGRect _titleUpperFrame;
    CGRect _titleOverlayFrame;
    
    UILabel *_cooldownLabel;
    CGRect _cooldownLowerFrame;
    CGRect _cooldownUpperFrame;
    
    UILabel *_spawnedLabel;
    CGRect _spawnedDefaultFrame;
    CGRect _spawnedOverlayFrame;
    
    UILabel *_teamLabel;
    CGRect _teamDefaultFrame;
    CGRect _teamOverlayFrame;
    
    NSTimer *_timer;
    CGFloat _maxCooldown;
    CGFloat _currentCooldown;
    NSString *_cooldownTitle;
    NSString *_teamOverlayText;
    NSString *_audioPrefix;
    UIColor *_normalColor;
    AVAudioPlayer *_audioPlayer;
    
    CGFloat _normalZPosition;
    CGFloat _parentLayerZPositionHide;
    CGFloat _overlayZPosition;
    
    dispatch_queue_t _sharedQueue;
}

@property (strong, atomic) AVSpeechSynthesizer *synthesizer;

- (void)cellSetupWithTitle:(NSString *)cooldownTitle
           teamOverlayText:(NSString *)teamOverlayText
               audioPrefix:(NSString *)audioPrefix
                  cooldown:(CGFloat)cooldown
               normalColor:(UIColor *)normalColor
               sharedQueue:sharedQueue
          audioSynthesizer:(AVSpeechSynthesizer *)synthesizer;

- (void) timerResetWithGlow:(BOOL)withGlow;
- (void) updateCooldownLabel;
- (void) updateFramesForOrientation:(UIInterfaceOrientation)orientation;

// WATK TEMP
- (void) tempChangeCooldown:(CGFloat)cooldown;
// WATK TEMP

- (void) viewOverlayFramesWithSound:(BOOL)sound;

@end
