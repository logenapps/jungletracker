//
//  AppDelegate.h
//  JungleTracker
//
//  Created by Logen Watkins on 4/20/13.
//  Copyright (c) 2013 Logen Watkins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) UINavigationController *superNavController;

@end
