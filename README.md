# Jungle Tracker #

This app was intended to be an easy-to-use aid for those in the "jungler" role of League of Legends, or for other roles that that want to enhance their in-game map awareness. 

### Features ###

* Clean, focused interface.
* "1-tap" timer starts and stops for all jungle camps -- no scrolling, scanning, or unnecessary fluff
* Visual cues include easy-to-read timers and (if selected by the user) larger visual cues when camps spawn
* Audio cues announcing upcoming camp respawns
* Users can modify visual and audio announcement preferences

### Tech used ###

* Objective-C, Xcode, iPhone simulator and actual device.
* CollectionViews are used for the main "grid", and were actually what I set out to try out when building this app.
* NSTimers are used for tracking cooldowns, determining when to notify the user with an audible or visible announcement.
* NSUserDefaults used for maintaining user preferences from use to use.

### Note on project status ###
* This project is not being maintained.
* The core functionality of this app (tracking friendly and enemy jungle timers) has been mostly integrated into the official game. While some functionality would still be useful (such as audio cues, forward announcements), most would just use in-game functionality now.